
#ifndef EXPRESSION_ABC_
#define EXPRESSION_ABC_

#include "TaskManager.h"
#include "LedStrip.h"
#include "Common.h"

#define BLACK 0.0f, 0.0f, 0.0f
#define WHITE 1.0f, 1.0f, 1.0f

class Expression
{
private:
    int port;
    int count;
    Strip *strip;
public:
    Expression(int port, TaskManager& manager)
    : port(port)
    , count(4)
    , strip(nullptr)
    {
        strip = new Strip(port, count);
    }
    
    void set(float red, float green, float blue)
    {
        for(size_t i = 0 ; i < count ; ++i)
        {
            strip->at(i).set(red, green, blue);
        }
    }
    
    void set(Direction direction, float red, float green, float blue)
    {
        set(BLACK);
        size_t index = 0;
        switch(direction)
        {
        case Direction::Up:
            index = 2;
            break;
        case Direction::Down:
            index = 0;
            break;
        case Direction::Left:
            index = 3;
            break;
        case Direction::Right:
            index = 1;
            break;
        default:
            return;
        }
        strip->at(2).set(red, green, blue);
    }
    
    void render()
    {
        strip->flush();
    }
};

#endif // EXPRESSION_ABC_