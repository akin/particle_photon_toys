
#ifndef SYSTEM_ABC_
#define SYSTEM_ABC_

#include "Common.h"
#include "TaskManager.h"
#include "Expression.h"
#include "ProximitySensor.h"
#include "EnviromentSensor.h"
#include "Store.h"

class System
{
public:
    TaskManager tasks;
    Expression expression;
    ProximitySensor proximity;
    EnviromentSensor enviroment;
    Store store;
};

#endif // SYSTEM_ABC_