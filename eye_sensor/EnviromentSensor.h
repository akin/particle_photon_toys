
#ifndef ENVIROMENT_SENSOR_ABC_
#define ENVIROMENT_SENSOR_ABC_

// https://github.com/drejkim/particle-weather-station
#include <Adafruit_DHT_Particle.h>
#include "TaskManager.h"

class EnviromentSensor
{
private:
    int port;
    int temperature;
    int humidity;
    DHT *dht;
public:
    EnviromentSensor(int port, TaskManager& manager)
    : port(port)
    , dht(nullptr)
    {
        manager.add(second, [this]{
            temperature = (int)dht->getTempCelcius();
            humidity = (int)dht->getHumidity();
        });
        
        dht = new DHT(port, DHT11);
        dht->begin();
    }
    
    float getTemperature()
    {
        return temperature;
    }
    
    float getHumidity()
    {
        return humidity;
    }
};

#endif // ENVIROMENT_SENSOR_ABC_