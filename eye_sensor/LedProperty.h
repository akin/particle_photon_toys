
#ifndef LED_PROPERTY_ABC_
#define LED_PROPERTY_ABC_

class Property
{
private:
    float cap(float value) const
    {
        if(value < 0.0f)
        {
            return 0.0f;
        }
        if(value > 1.0f)
        {
            return 255.0f;
        }
        return value * 255.0f;
    }
public:
    float r = 0.0f;
    float g = 0.0f;
    float b = 0.0f;
public:
    void set(float red, float green, float blue)
    {
        r = red;
        g = green;
        b = blue;
    }
    void add(float red, float green, float blue)
    {
        r += red;
        g += green;
        b += blue;
    }

    int red() const
    {
        return (int)cap(r);
    }
    int green() const
    {
        return (int)cap(g);
    }
    int blue() const
    {
        return (int)cap(b);
    }
};

#endif // LED_PROPERTY_ABC_
