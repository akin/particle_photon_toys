
#ifndef STORE_ABC_
#define STORE_ABC_

#include <string>
#include <unordered_map>
#include <assert.h>
#include "Common.h"

class Value
{
protected:
    const TypeID m_type;
    bool m_locked;
public:
    Value(TypeID type, bool locked) : m_type(type), m_locked(locked) {}
    virtual ~Value() {}
    const size_t type() const { return m_type; }
    bool locked() const { return m_locked; }
};

class Store
{
private:
    std::unordered_map<std::string, Value*> m_values;
public:
    template <class CType>
    CType& insert(const std::string& key, CType* item)
    {
        // item should not be null
        assert(item != nullptr);
        // should not be able to store values already in
        assert(m_values.find(key) == m_values.end());
        // types should match, if not, programmer error
        assert(item->type() == CType::ID);

        m_values[key] = item;

        return *item;
    }
    
    bool has(const std::string& key)
    {
        return m_values.find(key) != m_values.end();
    }
    
    // unstable function for getting items, GET or CREATE function.
    template <class CType>
    CType& get(const std::string& key)
    {
        auto iter = m_values.find(key);
        CType* tvalue;
        if(iter == m_values.end())
        {
            tvalue = new CType{};
            m_values[key] = tvalue;
            return *tvalue;
        }
        Value *value = iter->second;
        assert(value->type() == CType::ID);
        tvalue = static_cast<CType*>(iter->second);
        return *tvalue;
    }
};

#define DEFINE_VALUE_TYPE(CNAME, TYPE_ID, TYPE) \
    class Value##CNAME : public Value { \
    public: \
        const static TypeID ID = TYPE_ID;\
    private:\
        TYPE m_value;\
    public: \
        Value##CNAME(bool locked = false) : Value(ID, locked) {}\
        Value##CNAME(const TYPE& value, bool locked = false) : Value(ID, locked), m_value(value) {}\
        virtual ~Value##CNAME() {}\
        operator const TYPE&() const { return m_value; }\
        Value##CNAME& operator=(const TYPE& value) { assert(!locked()); if(!locked()) m_value = value; }\
    };

DEFINE_VALUE_TYPE(String, TypeID::String, std::string);
DEFINE_VALUE_TYPE(Boolean, TypeID::Boolean, bool);
DEFINE_VALUE_TYPE(Int32, TypeID::Int32, int32_t);
DEFINE_VALUE_TYPE(UInt32, TypeID::Uint32, uint32_t);
DEFINE_VALUE_TYPE(Float32, TypeID::Float32, float);
DEFINE_VALUE_TYPE(Float64, TypeID::Float64, double);
DEFINE_VALUE_TYPE(Path, TypeID::Path, std::string);
DEFINE_VALUE_TYPE(Url, TypeID::Url, std::string);

#endif // STORE_ABC_