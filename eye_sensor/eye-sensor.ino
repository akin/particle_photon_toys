
#include <Adafruit_DHT_Particle.h>
#include "State.h"

State state;
long last = 0;

void setup() 
{
    last = millis();
}

void loop() 
{
    long time = millis();
    long delta = time - last;
    last = time;
    state.update(delta);
}
