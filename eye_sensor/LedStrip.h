
#ifndef LED_STRIP_ABC_
#define LED_STRIP_ABC_

#include <neopixel.h>
#include <vector>
#include <math.h>
#include "LedProperty.h"

class Strip
{
private:
    Adafruit_NeoPixel m_unit;
    size_t m_size;
    std::vector<Property> m_properties;
public:
    Strip(uint8_t port, uint16_t size)
    : m_unit(size, port, WS2812)
    , m_size(size)
    {
        m_properties.resize(size);
    }
    
    Property& at(int index)
    {
        return m_properties[index];
    }
    
    void flush()
    {
        m_unit.begin();
        for(size_t i=0; i < m_size; ++i) 
        {
            const Property& property = m_properties[i];
            
            m_unit.setColor(i, property.red() , property.green(), property.blue());
        }
        m_unit.show();
    }
    
    void fade(float amount)
    {
        for(size_t i=0; i < m_size; ++i) 
        {
            Property& property = m_properties[i];
            property.add(-amount, -amount,-amount);
            
            if(property.r < 0.0f)
            {
                property.r = 0.0f;
            }
            if(property.g < 0.0f)
            {
                property.g = 0.0f;
            }
            if(property.b < 0.0f)
            {
                property.b = 0.0f;
            }
        }
    }
};

#endif // LED_STRIP_ABC_