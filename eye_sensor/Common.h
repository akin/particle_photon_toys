
#ifndef COMMON_ABC_
#define COMMON_ABC_

#include <cstdint>

static const size_t millisecond = 1;
static const size_t second = 1000 * millisecond;
static const size_t minute = 60 * second;
static const size_t hour = 60 * minute;
static const size_t day = 24 * hour;
static const size_t month = 30 * day;

enum class TypeID : uint32_t
{
    None = 0,
    String = 1,
    Int32 = 2,
    Uint32 = 3,
    Float32 = 4,
    Float64 = 5,
    Boolean = 6,
    Vec2i = 100,
    Vec2f = 101,
    Vec3i = 102,
    Vec3f = 103,
    Vec4i = 104,
    Vec4f = 105,
    Path = 201,
    Url = 202,
};

enum class Direction
{
    Up,
    Down,
    Left,
    Right
};

#endif // COMMON_ABC_