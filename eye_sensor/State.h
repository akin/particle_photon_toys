
#ifndef LED_STATE_ABC_
#define LED_STATE_ABC_

// https://github.com/drejkim/particle-weather-station
#include <cmath>
#include <functional>
#include <algorithm>
#include "TaskManager.h"
#include "ProximitySensor.h"
#include "EnviromentSensor.h"
#include "Expression.h"

class State
{
private:
    enum class Status
    {
        READY = 0,
        ALERT,
        RUNNING
    };
private:
    Status status;
    bool statusInit;
    int duration;
private:
    Expression expression;
    ProximitySensor proximity;
    EnviromentSensor enviroment;
    TaskManager tasks;
private:
    void changeState(Status value)
    {
        duration = 0;
        statusInit = true;
        status = value;
    }
    
    void ready()
    {
        if(statusInit)
        {
            // Reset lights
            expression.set(0,0,0);
            statusInit = false;
        }
        
        if(proximity.detected() && duration > 100 * millisecond)
        {
            changeState(Status::ALERT);
        }
    }
    
    void alert()
    {
        if(statusInit)
        {
            // Reset lights
            expression.set(0.01f, 0, 0);
            statusInit = false;
        }
        
        if(duration > 1000 * millisecond)
        {
            if(!proximity.detected())
            {
                changeState(Status::READY);
            }
            else
            {
                changeState(Status::RUNNING);
            }
        }
    }
    
    void running()
    {
        if(statusInit)
        {
            // Reset lights
            expression.set(0.01f,0.01f,0.01f);
            statusInit = false;
        }
        
        if(duration > 1000 * millisecond)
        {
            duration = 0;
            if(!proximity.detected())
            {
                changeState(Status::ALERT);
            }
        }
    }
public:
    State()
    : status(Status::READY)
    , expression(D4, tasks)
    , proximity(A0, tasks)
    , enviroment(D6, tasks)
    {
        changeState(Status::READY);
        
        tasks.add(50 * millisecond, [this]{
            switch(status)
            {
                case Status::READY:
                {
                    ready();
                    break;
                }
                case Status::ALERT:
                {
                    alert();
                    break;
                }
                case Status::RUNNING:
                {
                    running();
                    break;
                }
            }
        });
        
        tasks.add(100 * millisecond, [this]{
            expression.render();
        });
        
        // publish data about the state
        tasks.add(second, [this]{
            Particle.publish("state", 
                "temp:" + String(enviroment.getTemperature()) +
                ", hum: " + String(enviroment.getHumidity()) +
                ", prox: " + String(proximity.raw()),
                PRIVATE);
        });
    }
    
    void update(int delta)
    {
        duration += delta;
        tasks.update(delta);
        
        delay(5);
    }
};

#endif // LED_STATE_ABC_