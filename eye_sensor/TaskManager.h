
#ifndef TASKMANAGER_ABC_
#define TASKMANAGER_ABC_

#include <functional>
#include <algorithm>
#include "Common.h"
    
class PeriodicTask
{
public:
    size_t time;
protected:
    size_t updated;
    
    friend class TaskManager;
public:
    PeriodicTask(size_t time)
    : time(time)
    , updated(0)
    {
    }
    
    virtual ~PeriodicTask()
    {
    }
    
    virtual void run()
    {
    }
};

class LambdaTask : public PeriodicTask
{
public:
    std::function<void(void)> callback;
    
    LambdaTask(size_t time, const std::function<void()>& callback = {})
    : PeriodicTask(time)
    , callback(callback)
    {
    }
    
    virtual ~LambdaTask()
    {
    }
    
    virtual void run()
    {
        callback();
    }
};
    
class TaskManager
{
private:
    long duration = 0;
    std::vector<PeriodicTask*> tasks;
    
    void sortTasks()
    {
        // ascending sort
        std::sort(tasks.begin(), tasks.end(), [](const PeriodicTask* a, const PeriodicTask* b) -> bool {
            return a->time < b->time;
        });
    }
public:
    void add(PeriodicTask* task)
    {
        tasks.push_back(task);
        sortTasks();
    }
    
    void add(size_t interval, const std::function<void()>& callback)
    {
        add(new LambdaTask{interval, {callback}});
    }
    
    void update(int delta)
    {
        duration += delta;
        
        for(size_t i = 0 ; i < tasks.size() ; ++i)
        {
            auto& task = tasks[i];
            size_t current = duration / task->time;
            if(current != task->updated)
            {
                task->updated = current;
                task->run();
            }
        }
        
        if(duration > month)
        {
            // Well according to spec, the timer overflows after about a month, so
            // here we should probably just restart the device.
        }
    }
};

#endif // TASKMANAGER_ABC_