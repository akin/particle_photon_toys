
#ifndef SECURITY_ABC_
#define SECURITY_ABC_

#include "System.h"
#include "Common.h"

// Security interface, thats shown outside
class Security
{
protected:
    System& system;
    Security* m_parent = nullptr;
protected:
    virtual Security *createChild() = 0;
public:
    Security(System& system , Security* parent)
    : m_systen(system)
    , m_parent(parent)
    {
    }

    virtual ~Security() 
    {
        this->deactivate();
    }

    virtual void activate() = 0;
    virtual void deactivate() = 0;

    void upgrade()
    {
        Security* child = this->createChild();
        if(child != nullptr)
        {
            child->activate();
            this->deactivate();
        }
    }

    void downgrade()
    {
        if(m_parent != nullptr)
        {
            m_parent->activate();
            delete this;
        }
    }
};

class BottomState : public Security
{
protected:
    Security *createChild() override
    {
        return nullptr;
    }
public:
    BottomState(System& system)
    : Security(system, nullptr)
    {
        // Bottom state is special case, it needs to call activate.
        activate();
    }

    virtual ~BottomState()
    {
    }

    void activate() override
    {
        // register as the security layer
        // Push proximity sensor scans
    }

    void deactivate() override
    {
    }
};

#endif // SECURITY_ABC_