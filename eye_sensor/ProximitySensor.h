
#ifndef PROXIMITY_SENSOR_ABC_
#define PROXIMITY_SENSOR_ABC_

#include "TaskManager.h"

class ProximitySensor
{
private:
    int irPort;
    int ledPort = D7;
    
    int movementVal;
    bool movement = false;
public:
    ProximitySensor(int port, TaskManager& manager)
    : irPort(port)
    {
        pinMode(ledPort, OUTPUT);
        
        manager.add(10 * millisecond, [this](){
            movementVal = analogRead(irPort);
            movement = movementVal > 0; // should be 4095;
            
            if(movement)
            {
                digitalWrite(ledPort, HIGH);
            }
            else
            {
                digitalWrite(ledPort, LOW);
            }
        });
    }
    
    bool detected()
    {
        return movement;
    }
    
    int raw()
    {
        return movementVal;
    }
};

#endif // PROXIMITY_SENSOR_ABC_
